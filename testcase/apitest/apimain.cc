#include "json/LoadJSON.h"
#include "ApiExample/ApiExample.h"



using namespace testcase::Apimain;

static tabledeflist  m_tabledeflist[] =
{
    {"../../seedfile/Example.json", "../../seedfile/Example/Example.bin", EXAMPLE_TABLE_ID},
	{"../../seedfile/Example1.json", "../../seedfile/Example/Example1.bin", EXAMPLE_TABLE_ID},
	{"../../seedfile/Ctrldata_keys.json", "../../seedfile/Ctrldata/Ctrldata_keys.bin", CTRLDATA_KEYS_TABLE_ID},
	{"../../seedfile/Ctrldata_stats.json", "../../seedfile/Ctrldata/Ctrldata_stats.bin", CTRLDATA_STATS_TABLE_ID}

};

TEST(litealigndata, Expect0) 
{
	CAlignConfig::GetInstance()->Setlogpath("./output/log/");
	CLogHandler::GetInstance()->Init(true);
	CLoadJSON LoadJSON;
	tabledef AllTableDefine;
	tabledef bin_AllTableDefine[256];
	
	memset(bin_AllTableDefine, '\0', sizeof(struct tabledef) * 256);

	uint32_t num = sizeof(m_tabledeflist)/sizeof(tabledeflist);
	
	for(uint32_t i; i < num; i++)
	{
		memset(&AllTableDefine, '\0', sizeof(struct tabledef));
		LoadJSON.LoadJSONFile(m_tabledeflist[i].jsonfile, &AllTableDefine);
		LoadJSON.LoadBinaryFileJSON(m_tabledeflist[i].binfile, m_tabledeflist[i].table_id, bin_AllTableDefine);

        EXPECT_EQ(AllTableDefine.headLen, bin_AllTableDefine[i].headLen);
		EXPECT_EQ(AllTableDefine.table_id, bin_AllTableDefine[i].table_id);
		EXPECT_EQ(AllTableDefine.allLen, bin_AllTableDefine[i].allLen);
		EXPECT_EQ(AllTableDefine.totalLen, bin_AllTableDefine[i].totalLen);
		EXPECT_EQ(AllTableDefine.body_entry_num, bin_AllTableDefine[i].body_entry_num);
		EXPECT_EQ(AllTableDefine.local_key_pos, bin_AllTableDefine[i].local_key_pos);
		EXPECT_EQ(AllTableDefine.head_entry_num, bin_AllTableDefine[i].head_entry_num);

		for(uint32_t j = 0; j < AllTableDefine.head_entry_num; j++)
		{
			EXPECT_STREQ(AllTableDefine.head[j].szName, bin_AllTableDefine[i].head[j].szName);
			EXPECT_EQ(AllTableDefine.head[j].type, bin_AllTableDefine[i].head[j].type);
			EXPECT_EQ(AllTableDefine.head[j].len, bin_AllTableDefine[i].head[j].len);
		}

		for(uint32_t j = 0; j < AllTableDefine.body_entry_num; j++)
		{
			EXPECT_STREQ(AllTableDefine.tableinfo[j].szName, bin_AllTableDefine[i].tableinfo[j].szName);
			EXPECT_EQ(AllTableDefine.tableinfo[j].type, bin_AllTableDefine[i].tableinfo[j].type);
			EXPECT_EQ(AllTableDefine.tableinfo[j].len, bin_AllTableDefine[i].tableinfo[j].len);
			EXPECT_FALSE(bin_AllTableDefine[i].tableinfo[j].depenColumnFlag);
		}

		if(AllTableDefine.head)
			free((char*)(AllTableDefine.head));
		if(AllTableDefine.tableinfo)
			free((char*)(AllTableDefine.tableinfo));
		if(bin_AllTableDefine[i].head)
			free((char*)(bin_AllTableDefine[i].head));
		if(bin_AllTableDefine[i].tableinfo)
			free((char*)(bin_AllTableDefine[i].tableinfo));
	}
	litealigndata_destory(); 

}

int main(int argc, char **argv)
{
	printf("\n****************************************************\n");
	printf("\n*                                                  *\n");
	printf("\n*        ");
	printf("\033[0m\033[1;33mwelcome using litealigndata apitestcase\033[0m");
	printf("   *\n");
	printf("\n*                                                  *\n");
	printf("\n****************************************************\n");

	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}