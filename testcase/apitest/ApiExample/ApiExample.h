#ifndef _APIEXAMPLE_H_
#define _APIEXAMPLE_H_
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <spawn.h>
#include <dirent.h>
#include "ace/Log_Msg.h"
#include "gtest/gtest.h"
#include "log/LogHandler.h"
#include "sub/parser/XmlParser.h"
#include "conf/AlignConfig.h"
#include "struct/TableObject.h"

namespace testcase{

namespace Apimain{
    
    typedef struct
    {
        char jsonfile[1024];
        char binfile[1024];
        uint8_t  table_id;
    }tabledeflist;

};

}
#endif
