/*
 * =============================================================================
 *
 *       Filename:  Comm.cc
 *
 *       Description: 
 *
 *       Version:  1.0
 *       Created:  04/01, 2024
 *       Revision:  V1
 *       Compiler:  g++
 *
 *       Author:  keen.lee 
 *       Company
 *
 * =============================================================================
 */

#include "com/Comm.h"
namespace litealigndata
{

    NametypeEnDeFun nametype[] = 
    {
        {"AUTO_Invalid", AUTO_Invalid, 0, NULL, NULL},
        {"AUTO_UINT64", AUTO_UINT64, sizeof(uint64_t), NULL, NULL},
        {"AUTO_INT64", AUTO_INT64, sizeof(int64_t), NULL, NULL},
        {"AUTO_UINT32", AUTO_UINT32, sizeof(uint32_t), NULL, NULL},
        {"AUTO_INT32", AUTO_INT32, sizeof(int32_t), NULL, NULL},
        {"AUTO_UINT16", AUTO_UINT16, sizeof(uint16_t), NULL, NULL},
        {"AUTO_INT16", AUTO_INT16, sizeof(int16_t), NULL, NULL},
        {"AUTO_UINT8", AUTO_UINT8, sizeof(uint8_t), NULL, NULL},
        {"AUTO_INT8", AUTO_INT8, sizeof(int8_t), NULL, NULL},
        {"AUTO_IPV4", AUTO_IPV4, 0, NULL, NULL},
        {"AUTO_IPV6", AUTO_IPV6, 0, NULL, NULL},
        {"AUTO_IPV4V6", AUTO_IPV4V6, 0, NULL, NULL},
        {"AUTO_DateTime", AUTO_DateTime, 0, NULL, NULL},
        {"AUTO_ByteArray", AUTO_ByteArray, MAX_STR_LEN, NULL, NULL},
        {"AUTO_STRING", AUTO_STRING, MAX_STR_LEN, NULL, NULL},
        {"AUTO_IMSIBCD", AUTO_IMSIBCD, 0, NULL, NULL},
        {"AUTO_BCD", AUTO_BCD, 0, NULL, NULL},
        {"AUTO_TIMEVAL", AUTO_TIMEVAL, 0, NULL, NULL},
        {"AUTO_SP_U8_4", AUTO_SP_U8_4, 0, NULL, NULL},
        {"AUTO_SP_IPV6", AUTO_SP_IPV6, 0, NULL, NULL},
        {"AUTO_IMSI_TBCD", AUTO_IMSI_TBCD, 0, NULL, NULL},
        {"AUTO_IMEISV_BCD", AUTO_IMEISV_BCD, 0, NULL, NULL},
        {"AUTO_RealTime", AUTO_RealTime, sizeof(int64_t), NULL, NULL},
        {"AUTO_BCD_2Bytes", AUTO_BCD_2Bytes, 0, NULL, NULL},
        {"AUTO_IPV6_RSA_CU", AUTO_IPV6_RSA_CU, 0, NULL, NULL},
        {"AUTO_Metric_List", AUTO_Metric_List, 0, NULL, NULL},
        {"AUTO_RealTime_ms", AUTO_RealTime_ms, sizeof(int64_t), NULL, NULL},
        {"AUTO_IPV4_IPV6", AUTO_IPV4_IPV6, 0, NULL, NULL},
        {"AUTO_SharePointer_MAC_6Byte", AUTO_SharePointer_MAC_6Byte, 0, NULL, NULL},
        {"AUTO_IPV4_NET", AUTO_IPV4_NET, 0, NULL, NULL},
        {"AUTO_IPV6_NET", AUTO_IPV6_NET, 0, NULL, NULL},
        {"AUTO_STRING_NET", AUTO_STRING_NET, 0, NULL, NULL},
        {"AUTO_STRING_NET", AUTO_STRING_NET, 0, NULL, NULL},
        {"AUTO_Struct", AUTO_STRUCT, 0, NULL, NULL},
        {"AUTO_Union", AUTO_UNION, 0, NULL, NULL},
        {"AUTO_MAX", AUTO_MAX, 0, NULL, NULL},

    };
    NametypeEnDeFun* findNametype(char* name)
    {
        for(u_int32_t i = 0; i < (sizeof(nametype))/(sizeof(NametypeEnDeFun)); i++)
        {
            if(!strcmp(name, nametype[i].name))
                return &nametype[i];
        }
        return NULL;
    };
    autoType findNameautotype(char* name)
    {
        for(u_int32_t i = 0; i < (sizeof(nametype))/(sizeof(NametypeEnDeFun)); i++)
        {
            if(!strcmp(name, nametype[i].name))
                return nametype[i].type;
        }
        return AUTO_Invalid;
    };
    uint16_t findNamelen(autoType type)
    {
        for(u_int32_t i = 0; i < (sizeof(nametype))/(sizeof(NametypeEnDeFun)); i++)
        {
            if(type == nametype[i].type)
                return nametype[i].len;
        }
        return 0;
    };
    uint16_t findNametypeLen(char* name)
    {
        for(u_int32_t i = 0; i < (sizeof(nametype))/(sizeof(NametypeEnDeFun)); i++)
        {
            if(!strcmp(name, nametype[i].name))
                return nametype[i].len;
        }
        return 0;
    };
}