/*
 * =============================================================================
 *
 *       Filename:  LoadJSON.cc
 *
 *       Description: 
 *
 *       Version:  1.0
 *       Created:  04/01, 2024
 *       Revision:  V1
 *       Compiler:  g++
 *
 *       Author:  keen.lee 
 *       Company:  
 *
 * =============================================================================
 */
#include "LoadJSON.h"
#include "file/Example/Example.h"

bool CLoadJSON::LoadJSONFile(const char * jsonfile, struct  tabledef* AllTableDefine)
{
	FILE* fp = fopen( jsonfile, "r" );
	if ( fp == NULL )
	{
		ACE_DEBUG((LM_ERROR, "current json does not exist: %s.\n", jsonfile));
		return false;
	}

	fseek( fp, 0, SEEK_END );
	long filelen = ftell( fp );
	fseek( fp, 0, SEEK_SET );
	char* jsonBuf=(char*)malloc( filelen + 1 );
	if ( jsonBuf == NULL )
	{
		ACE_DEBUG((LM_ERROR, "malloc jsonBuf memory failed.\n"));
		return false;
	}

	fread( jsonBuf, 1, filelen, fp );
	fclose(fp);	
	cJSON* jsonSeedData = cJSON_Parse( jsonBuf );
	cJSON* item = cJSON_GetObjectItem(jsonSeedData, "protocols");
	char* protocols = item->valuestring;

	int table_id = 0;
    item = cJSON_GetObjectItem(jsonSeedData, "tableid");
	table_id = item->valueint;

	uint32_t local_key_pos = 0;
	item = cJSON_GetObjectItem(jsonSeedData, "local_key_pos");
	sscanf(item->valuestring, "%x", &local_key_pos);

	cJSON *field = NULL;
	cJSON* pItem = NULL;
	char* name = NULL;
	char* type = NULL;
	int   offset = 0;

    uint64_t  len = 0;
	item = cJSON_GetObjectItem(jsonSeedData, "head");
	uint16_t head_entry_num = cJSON_GetArraySize(item);
	AllTableDefine->head_entry_num = head_entry_num;
	AllTableDefine->head = NULL;
	if(head_entry_num > 0)
	{
		AllTableDefine->head = (ptrTabDef)malloc(sizeof(struct TableDefine)*head_entry_num);
		memset(AllTableDefine->head, '\0', sizeof(struct TableDefine)*head_entry_num);
		for(int i = 0; i < head_entry_num;i++)
		{
			field = cJSON_GetArrayItem(item, i);

			pItem = cJSON_GetObjectItem(field, "name");
			name = pItem->valuestring;
			
			strncpy(AllTableDefine->head[i].szName, name, sizeof(AllTableDefine->head[i].szName));

			pItem = cJSON_GetObjectItem(field, "type");
			type = pItem->valuestring;
			litealigndata::NametypeEnDeFun *typelen = litealigndata::findNametype(type);
			AllTableDefine->head[i].type = typelen->type;

			pItem = cJSON_GetObjectItem(field, "offset");
			//offset = GetOffset(itemtype);
			offset = typelen->len;
			AllTableDefine->head[i].len = len;
			len += offset;
		}
		
	}

	AllTableDefine->headLen = len;

	len = 0;
	
	uint8_t fields = GetBodyfieldsNum(jsonSeedData);
	AllTableDefine->body_entry_num = fields;
	AllTableDefine->tableinfo = NULL;
	AllTableDefine->tableinfo = (ptrTabDef)malloc(sizeof(struct TableDefine)*fields);
	memset(AllTableDefine->tableinfo, '\0', sizeof(struct TableDefine)*fields);

	for (size_t i = 0; i < m_item.size(); ++i) 
	{
		strncpy(AllTableDefine->tableinfo[i].szName, m_item[i].szName, sizeof(AllTableDefine->tableinfo[i].szName));
		AllTableDefine->tableinfo[i].type = m_item[i].type;
		AllTableDefine->tableinfo[i].len = m_item[i].len;
		AllTableDefine->tableinfo[i].depenColumnFlag = false;
		len += m_item[i].len;
		
		//std::cout<< "m_item[" << i << "].szName:  " << m_item[i].szName << "\n" << "m_item[" << i << 
		//"].len:  " << m_item[i].len << "\n" << "m_item[" << i << "].type:  " << m_item[i].type << "\n" 
		//<< "m_item[" << i << "].sublen:  " << m_item[i].sublen << std::endl;
	}

    AllTableDefine->table_id = table_id;
	AllTableDefine->totalLen = len;   //body total len
	AllTableDefine->local_key_pos = local_key_pos;
	AllTableDefine->allLen = sizeof(struct  tabledef) - sizeof(char*) + sizeof(struct TableDefine)*fields
						 - sizeof(char*) + sizeof(struct TableDefine)*head_entry_num;

	cJSON_Delete(jsonSeedData);
	free(jsonBuf);
 
    ACE_DEBUG((LM_INFO, "JSONFileToBinaryFile: JSONFile [%s] success\n", jsonfile));

	ACE_DEBUG((LM_DEBUG,"AllTableDefine->head_entry_num: [%u]\nAllTableDefine->table_id: [%u]\nAllTableDefine->allLen: [%u]\nAllTableDefine->totalLen: [%u]\nAllTableDefine->body_entry_num: [%u]\nAllTableDefine->local_key_pos: [%u]\n",\
	AllTableDefine->head_entry_num,\
	AllTableDefine->table_id,\
    AllTableDefine->allLen,\
    AllTableDefine->totalLen,\
    AllTableDefine->body_entry_num,\
	AllTableDefine->local_key_pos));

    for(int i = 0; i < AllTableDefine->head_entry_num; i++)
	{
		ACE_DEBUG((LM_DEBUG,"AllTableDefine->head[%d].szName: [%s]\nAllTableDefine->head[%d].type: [%d]\nAllTableDefine->head[%d].len: [%u]\n",\
		i, AllTableDefine->head[i].szName,\
		i, AllTableDefine->head[i].type,\
		i, AllTableDefine->head[i].len));
	}
    
    for(int i = 0; i < AllTableDefine->body_entry_num; i++)
	{
		ACE_DEBUG((LM_DEBUG,"AllTableDefine->tableinfo[%d].szName: [%s]\nAllTableDefine->tableinfo[%d].type: [%d]\nAllTableDefine->tableinfo[%d].len: [%u]\n\
		AllTableDefine->tableinfo[%d].depenColumnFlag: [%d]\n",\
		i, AllTableDefine->tableinfo[i].szName,\
		i, AllTableDefine->tableinfo[i].type,\
		i, AllTableDefine->tableinfo[i].len,\
		i, AllTableDefine->tableinfo[i].depenColumnFlag));

	}

	return true;
}

bool CLoadJSON::JSONFileToBinaryFile(const char * jsonfile)
{
	struct  tabledef AllTableDefine;
    LoadJSONFile(jsonfile, &AllTableDefine);
    std::string BinaryFile = jsonfile;
	char* ptr = (char*)(&AllTableDefine);

	BinaryFile.replace(BinaryFile.find("json"), strlen("json"), "bin");

	FILE* fp = fopen( BinaryFile.c_str(), "w" );
    
	if(!fp)
	{
		return false;
	}

    fwrite(ptr, sizeof(struct  tabledef) - 2*sizeof(char*), 1, fp);

    for(int i = 0; i < AllTableDefine.head_entry_num; i++)
	{
        fwrite((char*)(&AllTableDefine.head[i]), sizeof(AllTableDefine.head[i]), 1, fp);
	}

    for(int i = 0; i < AllTableDefine.body_entry_num; i++)
	{
        fwrite((char*)(&AllTableDefine.tableinfo[i]), sizeof(AllTableDefine.tableinfo[i]), 1, fp);
	}

    ACE_DEBUG((LM_INFO, "JSONFileToBinaryFile: JSONFile [%s], BinaryFile [%s]\n", 
	jsonfile, BinaryFile.c_str()));
    fclose(fp);
 
	if(AllTableDefine.head)
		free((char*)(AllTableDefine.head));
	if(AllTableDefine.tableinfo)
		free((char*)(AllTableDefine.tableinfo));

	return true;
}
bool CLoadJSON::LoadBinaryFileJSON(const char * binaryfile, uint8_t&  table_id,
		 struct  tabledef* AllTableDefine)
{
	FILE* fp = fopen( binaryfile, "r" );
	if ( fp == NULL )
	{
		ACE_DEBUG((LM_ERROR, "current binaryfile does not exist: %s.\n", binaryfile));
		return false;
	}

	fseek( fp, 0, SEEK_END );
	long filelen = ftell( fp );
	fseek( fp, 0, SEEK_SET );
	char* binaryBuf=(char*)malloc( filelen + 1 );
	if ( binaryBuf == NULL )
	{
		ACE_DEBUG((LM_ERROR, "malloc binaryBuf memory failed.\n"));
		return false;
	}

	fread( binaryBuf, 1, filelen, fp );
	table_id = ((struct  tabledef*)binaryBuf)->table_id;
    AllTableDefine[table_id] = *((struct  tabledef*)binaryBuf);
	struct  tabledef* TableDefine = &AllTableDefine[table_id];
	uint16_t offset = sizeof(struct  tabledef) - 2*sizeof(char*);
	TableDefine->head = NULL;
	if(TableDefine->head_entry_num> 0)
	{
		TableDefine->head = 
		(ptrTabDef)malloc((sizeof(struct TableDefine))*(TableDefine->head_entry_num));
		memcpy((char*)(TableDefine->head), binaryBuf + offset, 
		(sizeof(struct TableDefine))*(TableDefine->head_entry_num));

	}
	TableDefine->tableinfo = NULL;
	if(TableDefine->body_entry_num > 0)
	{
		TableDefine->tableinfo = 
		(ptrTabDef)malloc((sizeof(struct TableDefine))*(TableDefine->body_entry_num));
		memcpy((char*)(TableDefine->tableinfo), 
				binaryBuf + offset + (sizeof(struct TableDefine))*(TableDefine->head_entry_num),
				(sizeof(struct TableDefine))*(TableDefine->body_entry_num));
	}

	fclose(fp);	
	free(binaryBuf);
   
    ACE_DEBUG((LM_INFO, "LoadBinaryFileJSON: BinaryFile [%s]\n", binaryfile));

	ACE_DEBUG((LM_DEBUG,"TableDefine->head_entry_num: [%u]\nTableDefine->table_id: [%u]\nTableDefine->allLen: [%u]\nTableDefine->totalLen: [%u]\nTableDefine->body_entry_num: [%u]\nTableDefine->local_key_pos: [%u]\n",\
	TableDefine->head_entry_num,\
	TableDefine->table_id,\
    TableDefine->allLen,\
    TableDefine->totalLen,\
    TableDefine->body_entry_num,\
	TableDefine->local_key_pos));

    for(int i = 0; i < TableDefine->head_entry_num; i++)
	{
		ACE_DEBUG((LM_DEBUG,"TableDefine->head[%d].szName: [%s]\nTableDefine->head[%d].type: [%d]\nTableDefine->head[%d].len: [%u]\n",\
		i, TableDefine->head[i].szName,\
		i, TableDefine->head[i].type,\
		i, TableDefine->head[i].len));
	}
    
    for(int i = 0; i < TableDefine->body_entry_num; i++)
	{
		ACE_DEBUG((LM_DEBUG,"TableDefine->tableinfo[%d].szName: [%s]\nTableDefine->tableinfo[%d].type: [%d]\nTableDefine->tableinfo[%d].len: [%u]\n",\
		i, TableDefine->tableinfo[i].szName,\
		i, TableDefine->tableinfo[i].type,\
		i, TableDefine->tableinfo[i].len));

	}
	return true;
}
void CLoadJSON::GetGetObjectItem(itemstruct& itemData, struct TableDefine& itemInfo)
{
	char*  type   = NULL;
	char*  name   = NULL;
	int    offset = 0;
	cJSON* field  = NULL;
	cJSON* pItem  = NULL;
	
	field = cJSON_GetArrayItem(&itemData.item, itemData.currentIndex);
	pItem = cJSON_GetObjectItem(field, "name");
	name = pItem->valuestring;
	strncpy(itemInfo.szName, name, sizeof(itemInfo.szName));

	pItem = cJSON_GetObjectItem(field, "type");
	type = pItem->valuestring;
	litealigndata::NametypeEnDeFun *typelen = litealigndata::findNametype(type);
	itemInfo.type = typelen->type;

	pItem = cJSON_GetObjectItem(field, "offset");
	offset = typelen->len;
	itemInfo.len = offset;
};
uint8_t CLoadJSON::GetBodyfieldsNum(cJSON* jsonSeedData)
{
	cJSON* pItem = NULL;
	cJSON* field = NULL;

	pItem = cJSON_GetObjectItem(jsonSeedData, "fields");
	uint8_t fields = cJSON_GetArraySize(pItem);
	uint8_t currentIndex = 0;
	itemstruct itemData;
	itemData.currentIndex = 0;
	itemData.totalIndex = fields;
	memcpy(&itemData.item, pItem, sizeof(cJSON));
	m_queue.push_back(itemData);

	while(1)
	{
		memset(&itemData, '\0', sizeof(itemstruct));
		if(m_queue.empty())
			break;
		itemData = m_queue.back();
		m_queue.pop_back();
		struct TableDefine itemInfo;

		while(1)
		{
			if(itemData.currentIndex >= itemData.totalIndex)
			{
				char* name = itemData.item.prev->prev->valuestring;
				char* type = (itemData.item.prev)->valuestring;
				struct TableDefine itemInfoEnd;
				memset(&itemInfoEnd, '\0', sizeof(struct TableDefine));
				char buf[128] = "\0";
				snprintf(buf, 128, "%s %s  %s", type, name, "   End");
				strncpy(itemInfoEnd.szName, buf, sizeof(itemInfoEnd.szName));
				m_item.push_back(itemInfoEnd);
				break;
			}
				
			memset(&itemInfo, '\0', sizeof(struct TableDefine));
			GetGetObjectItem(itemData, itemInfo);
			m_item.push_back(itemInfo);
			itemData.currentIndex++;

			if(itemInfo.type == litealigndata::AUTO_STRUCT || 
						itemInfo.type == litealigndata::AUTO_UNION)
			{
				m_queue.push_back(itemData);
				cJSON* field = cJSON_GetArrayItem(&itemData.item, itemData.currentIndex-1);
				cJSON* pItem = cJSON_GetObjectItem(field, "offset");
				fields = cJSON_GetArraySize(pItem);

				{
					itemInfo = m_item.back();
					m_item.pop_back();
					itemInfo.sublen = fields;
					m_item.push_back(itemInfo);
				}

				memset(&itemData, '\0', sizeof(itemData));

				itemData.totalIndex = fields;
				memcpy(&itemData.item, pItem, sizeof(cJSON));
				itemData.currentIndex = 0;
				m_queue.push_back(itemData);
				break;
			}
		
		}

	}
	for(auto item: m_item)
	{
		ACE_DEBUG((LM_DEBUG,"item.szName:  %s, item.len:  %d, item.type:  %d", 
		item.szName, item.len, item.type));
		//std::cout<< "item.szName:  " << item.szName << "\n" << "item.len:  " << item.len << "\n"
		//<< "item.type:  " << item.type << "\n" << std::endl;
	}
	return m_item.size();
};