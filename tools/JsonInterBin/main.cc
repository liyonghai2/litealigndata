/*
 * =============================================================================
 *
 *       Filename:  main.cc
 *
 *       Description: 
 *
 *       Version:  1.0
 *       Created:  04/01, 2024
 *       Revision:  V1
 *       Compiler:  g++
 *
 *       Author:  keen.lee 
 *       Company:  
 *
 * =============================================================================
 */

#include "json/LoadJSON.h"
#include "log/LogHandler.h"
#include "conf/AlignConfig.h"
#include "com/AlignDataTypeDef.h"

typedef struct _tabledeflist
{
    char jsonfile[1024];
    char binfile[1024];
     uint8_t  table_id;
}tabledeflist;

static tabledeflist  m_tabledeflist[] =
{
    // {"../seedfile/Example.json", "../seedfile/Example.bin", EXAMPLE_TABLE_ID},
    // {"../seedfile/Example1.json", "../seedfile/Example1.bin", EXAMPLE_TABLE_ID_1},
    {"../seedfile/Ctrldata_keys.json", "../seedfile/Ctrldata_keys.bin", CTRLDATA_KEYS_TABLE_ID},
    // {"../seedfile/Ctrldata_stats.json", "../seedfile/Ctrldata_stats.bin", CTRLDATA_STATS_TABLE_ID}

};

int main(int argc, char* argv[])
{
    CAlignConfig::GetInstance()->Setlogpath("./output/log/");
    CLogHandler::GetInstance()->Init(true);
    CLoadJSON m_CLoadJSON;
    struct  tabledef AllTableDefine[256];
    for(uint32_t i = 0; i < sizeof(m_tabledeflist)/sizeof(tabledeflist); i++)
    {
        m_CLoadJSON.JSONFileToBinaryFile(m_tabledeflist[i].jsonfile);
        uint8_t table_id = m_tabledeflist[i].table_id;
        m_CLoadJSON.LoadBinaryFileJSON(m_tabledeflist[i].binfile, table_id, AllTableDefine);
		if(AllTableDefine[i].head)
			free((char*)(AllTableDefine[i].head));
		if(AllTableDefine[i].tableinfo)
			free((char*)(AllTableDefine[i].tableinfo));        
    }
    litealigndata_destory(); 
    

    return 0;
}