#ifndef _LOADJSON_H_
#define _LOADJSON_H_

#include <stdio.h>
#include <stdlib.h>
#include "cjson/cJSON.h"
#include "ace/Log_Msg.h"
#include "ace/Log_Record.h"
#include "ace/Log_Priority.h"
#include "com/AlignDataTypeDef.h"

class CLoadJSON
{
    public:
        typedef struct _itemstruct
        {
            cJSON item;
            uint8_t currentIndex;
            uint8_t totalIndex;

        }itemstruct;

        CLoadJSON()
        {

        };
        void GetGetObjectItem(itemstruct& itemData, struct TableDefine& itemInfo);
        uint8_t GetBodyfieldsNum(cJSON* jsonSeedData);
        bool JSONFileToBinaryFile(const char * jsonfile);
        bool LoadBinaryFileJSON(const char * jsonfile, uint8_t&  table_id, 
            struct  tabledef* AllTableDefine);
        bool LoadJSONFile(const char * jsonfile, struct  tabledef* AllTableDefine);
        ~CLoadJSON()
        {

        };
    private:
        std::vector<itemstruct> m_queue;
        std::vector<struct TableDefine> m_item;

};


#endif
