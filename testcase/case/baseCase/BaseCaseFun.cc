/*
 * =============================================================================
 *
 *       Filename:  BaseCaseFun.cc
 *
 *       Description: 
 *
 *       Version:  1.0
 *       Created:  04/01, 2024
 *       Revision:  V1
 *       Compiler:  g++
 *
 *       Author:  keen.lee 
 *       Company
 *
 * =============================================================================
 */
#include "BaseCase.h"

using namespace testcase::CaseBaseCase;

bool CBaseCaseFun::MethodFun(std::map<std::string, litealigndata::autoType>& item, 
        CColumnObject* release_column_obj, std::string dirname, CBaseFileFormat *fileFormat)
{
    return true;
}